<nav class="navbar navbar-pasific navbar-mp megamenu navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="{{ asset('assets/img/logo/logo-default.png') }}" alt="logo">
                GAME OF BLOGS
            </a>
        </div>

        <div class="navbar-collapse collapse navbar-main-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('welcome') }}">Home</a>
                </li>
            
                @auth
                <li>
                    <a href="{{ route('home') }}">Dashboard</a>
                </li>
                @else
                <li>
                    <a href="{{ route('login') }}">Login</a>
                </li>
                @endauth

            </ul>

        </div>
    </div>
</nav>
