<?php

namespace App\DTO;

use App\Category;
use App\Post;
use DateTime;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class PostDto extends DataTransferObject{
    public int $id;
    public string $title;
    public string $excerpt;
    public string $content;
    public string $image;
    public ?string $published_at;
    public CategoryDto $categoryDto;
    public UserDto $userDto;
    public ?Collection $tagDtoCollection;

    public function getTagIds(): array{
        $tagIds = [];
        foreach($this->tagDtoCollection as $tagDto){
            array_push($tagIds, $tagDto->id);
        }
        return $tagIds;
    }
    

    // public function __construct(
    //     int $id,
    //     string $title,
    //     string $excerpt,
    //     string $content,
    //     string $image,
    //     ?string $published_at,
    //     int $category_id,
    //     int $user_id,
    //     ?Collection $tagIds
    // ) {
    //     $this->id = $id;
    //     $this->title = $title;
    //     $this->image = $image;
    //     $this->excerpt = $excerpt;
    //     $this->content = $content;
    //     $this->published_at = $published_at;
    //     $this->category_id = $category_id;
    //     $this->user_id = $user_id;
    //     $this->tagIds = $tagIds;
    // }

    // public function toArray(): array
    // {
    //     // $tags = [];
    //     // foreach($this->tagDtoCollection as $tagDto){
    //     //     $tags[] = $tagDto->toArray();
    //     // }
    //     return [
    //         'title' => $this->title,
    //         'excerpt' => $this->excerpt,
    //         'content' => $this->content,
    //         'image' => $this->image,
    //         'published_at' => $this->published_at,
    //         'category_id' => $this->category_id,
    //         'user_id' => $this->user_id
    //     ];

    // }


}

?>