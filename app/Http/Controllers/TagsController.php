<?php

namespace App\Http\Controllers;

use App\DTO\TagDto;
use App\Http\Requests\Tags\CreateTagRequest;
use App\Http\Requests\Tags\UpdateTagRequest;
use App\Services\TagService;
use App\Tag;
use Exception;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    private TagService $tagService;
    public function __construct(TagService $ref)
    {
        $this->tagService = $ref;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = $this->tagService->getTags();        
        return view('tags.index', compact([
            'tags'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        // Tag::create([
        //     'name'=>$request->name
        // ]);

        $tagDto = new TagDto(id: 0, name: $request->name);
        try{
            $this->tagService->create($tagDto);
        }catch(Exception $e){
            session()->flash('error', $e->getMessage());
            return redirect(route('tags.index'));
        }
        
        session()->flash('success', 'Tag Added Successfully!');
        return redirect(route('tags.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact([
            'tag'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tagDto = new TagDto(id: $tag->id, name: $request->name);
        
        try{
            $this->tagService->update($tagDto, $tag);
        }catch(Exception $e){
            session()->flash('error', $e->getMessage());
            return redirect(route('tags.index'));
        }
        // $tag->name = $request->name;
        // $tag->save();

        session()->flash('success', 'Tag Updated Successfully!');
        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag->posts->count() > 0){
            session()->flash('error', 'Tag cannot be deleted as it is associated with some post!');
            return redirect()->back();
        }
        $tag->delete();
        session()->flash('success', "Tag Deleted Successfully");
        return redirect(route('tags.index'));
    }
}
