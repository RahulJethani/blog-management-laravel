<?php

namespace App;

use App\Casters\CategoryCollectionCaster;
use App\DTO\CategoryDto;
use App\Helpers\Utils;
use App\Interfaces\CategoryConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Category extends Model implements CategoryConstants
{
    protected $fillable = ['name'];

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public static function persistCategory(CategoryDto $categoryDto): self
    {

        // 1. validate
        Utils::validateOrThrow(self::CREATE_RULES, $categoryDto->toArray());
        // 2. create
        $category = null;
        DB::transaction(function() use($categoryDto, &$category) {
            $category = Category::create($categoryDto->toArray());
        });
        return $category;
    }

    public function updateCategory(CategoryDto $categoryDto): self
    {

        // 1. validate
        Utils::validateOrThrow(array_merge(self::UPDATE_RULES, ['name' => 'unique:categories,name,'.$categoryDto->id]), $categoryDto->toArray());
        // 2. create
        $category = null;
        DB::transaction(function() use($categoryDto, &$category) {
            $this->name = $categoryDto->name;
            $this->save();
        });
        return $this;
    }



    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }

}
