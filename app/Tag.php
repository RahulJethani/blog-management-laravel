<?php

namespace App;

use App\Casters\TagCollectionCaster;
use App\DTO\TagDto;
use App\Helpers\Utils;
use App\Interfaces\TagConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model implements TagConstants
{
    protected $fillable = ['name'];

    public function posts(){
        return $this->belongsToMany(Post::class)->withTimestamps();
    }

    public static function persistTag(TagDto $tagDto): Tag
    {

        // 1. validate
        Utils::validateOrThrow(self::CREATE_RULES, $tagDto->toArray());
        // 2. create
        $tag = null;
        DB::transaction(function() use($tagDto, &$tag) {
            $tag = Tag::create($tagDto->toArray());
        });
        return $tag;
    }

    public function updateTag(TagDto $tagDto): self
    {
        // 1. validate
        Utils::validateOrThrow(array_merge(Tag::getUpdateValidationRules(), ['name' => 'unique:tags,name,'.$tagDto->id]), $tagDto->toArray());
        // 2. update
        
        DB::transaction(function() use($tagDto) {
            $this->name = $tagDto->name;
            $this->save();
        });
        return $this;
    }

    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }

}

