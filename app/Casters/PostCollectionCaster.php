<?php

namespace App\Casters;

use App\Category;
use App\Collections\CollectionOfPost;
use App\DTO\CategoryDto;
use App\DTO\PostDto;
use App\DTO\UserDto;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Caster;

class PostCollectionCaster implements Caster
{
    public function cast(mixed $value): CollectionOfPost
    {
        
        return new CollectionOfPost(array_map(function(array $data){
            
            return new PostDto(
                id: $data['id'],
                title: $data['title'], 
                excerpt: $data['excerpt'], 
                content: $data['content'], 
                image: $data['image'], 
                published_at: $data['published_at'], 
                categoryDto: new CategoryDto(
                    $data['category']
                ), 
                userDto: new UserDto(
                    $data['author']
                )
            );
        }, $value->toArray()));
    }
}
?>