<?php

namespace App\Casters;

use App\Collections\CollectionOfCategory;
use App\DTO\CategoryDto;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Caster;

class CategoryCollectionCaster implements Caster
{
    
    public function cast(mixed $value): CollectionOfCategory
    {
        return new CollectionOfCategory(array_map(function(array $data){
            return new CategoryDto(...$data);
        }, $value->toArray()));
    }
}
?>