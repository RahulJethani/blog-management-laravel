<?php

namespace App;

use App\Casters\PostCollectionCaster;
use App\Collections\CollectionOfPost;
use App\DTO\PostDto;
use App\Helpers\Utils;
use App\Interfaces\PostConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Post extends Model implements PostConstants
{
    use SoftDeletes;
    protected $dates = [
        'published_at'
    ];
    protected $fillable = [
        'title',
        'excerpt',
        'content',
        'image',
        'published_at',
        'category_id',
        'user_id'
    ];

    // Helper Functions
    public function deleteImage(){
        Storage::delete($this->image);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function hasTag(Tag $tag){
        return in_array($tag->id, $this->tags->pluck('id')->toArray());
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopePublished($query){
        return $query->where('published_at', '<=', now());
    }

    public function scopeSearch($query){
        $search = request('search');
        if($search){
            return $query->where('title', 'like', "%{$search}%");
        }
        return $query;
    }

    public static function persistPost(PostDto $postDto): Post
    {

        // 1. validate
        Utils::validateOrThrow(array_diff_key(self::CREATE_RULES, ['image' => '']), $postDto->toArray());
        // 2. create
        $post = null;
        DB::transaction(function() use($postDto,&$post) {
            $dtoArray = $postDto->toArray();
            $insertArray = array_merge([ 
                'category_id' => $postDto->categoryDto->id,
                'user_id' => $postDto->userDto->id
            ], $dtoArray);
            $post = Post::create($insertArray);
            $post->tags()->attach($postDto->getTagIds());
        });
        return $post;
    }

    public function updatePost(PostDto $postDto): self
    {
        // 1. validate
        Utils::validateOrThrow(array_merge(array_diff_key(self::UPDATE_RULES, ['image' => '']), ['title' => 'unique:posts,title,'.$postDto->id]), $postDto->toArray());
        // 2. Update
        $post = null;
        DB::transaction(function() use($postDto,&$post) {
            
            $data = $postDto->toArray();
            $updateArray = array_merge([ 
                'category_id' => $postDto->categoryDto->id,
                'user_id' => $postDto->userDto->id
            ], $data);
            
            $this->update($updateArray);
            $this->tags()->sync($postDto->getTagIds());
             
        });
        return $this;


    }


    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULES;
    }

    public static function getUpdateValidationRules(): array
    {
        return self::UPDATE_RULES;
    }

    
}
