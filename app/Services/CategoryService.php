<?php


namespace App\Services;

use App\Casters\CategoryCollectionCaster;
use App\Category;
use App\Collections\CollectionOfCategory;
use App\DTO\CategoryDto;
use Illuminate\Support\Collection;

class CategoryService
{
    public function create(CategoryDto $categoryDto)
    {
        Category::persistCategory($categoryDto);
    }

    public function update(CategoryDto $categoryDto, Category $category)
    {
        $category->updateCategory($categoryDto);
    }

    public function getCategories(): CollectionOfCategory{
        $categories = Category::all();
        $categoryDtoCollection = (new CategoryCollectionCaster())->cast($categories);

        return $categoryDtoCollection;
    }
}
